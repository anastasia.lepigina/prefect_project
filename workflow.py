import prefect
from prefect import task, Flow
from prefect.storage import GitLab
from prefect.run_configs import KubernetesRun
from prefect.executors import LocalExecutor


gitlab_storage = GitLab(
    repo="anastasia.lepigina/prefect_project",
    path="workflow.py",
    ref="main",
    access_token_secret="GITLAB_ACCESS_TOKEN"
)

kubernetes_run_config = KubernetesRun(image="lpgn/prefect_try:latest")
local_executor = LocalExecutor()


@task(nout=3)
def preprocessing():
    import s3fs
    import cloudpickle
    import torch
    import torch.nn as nn
    import pandas as pd
    from sklearn.preprocessing import MinMaxScaler

    logger = prefect.context.get("logger")
    fs = s3fs.S3FileSystem(anon=False, key='AKIA5KGI53PNE46SAFBX', secret='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')
    fs.download(rpath='s3://actionengine-ml/LSTM/final_forecasts.parq', lpath='final_forecasts.parq')
    dataset = pd.read_parquet('final_forecasts.parq', engine='fastparquet')
    dataset['ds'] = pd.to_datetime(dataset['ds'])
    train_data = dataset['Occupancy Rate mean'][:-24].to_numpy()
    test_data = dataset['Occupancy Rate mean'][-24:-12].to_numpy()
    scaler = MinMaxScaler(feature_range=(-1, 1))
    train_data_normalized = scaler.fit_transform(train_data.reshape(-1, 1))
    train_data_normalized = torch.FloatTensor(train_data_normalized).view(-1)

    train_window = 12

    def create_inout_sequences(input_data, tw):
        inout_seq = []
        L = len(input_data)
        for i in range(L - tw):
            train_seq = input_data[i:i + tw]
            train_label = input_data[i + 1:i + tw + 1]
            inout_seq.append((train_seq, train_label))
        return inout_seq

    train_inout_seq = create_inout_sequences(train_data_normalized, train_window)
    train_inout_seq_pickled = cloudpickle.dumps(train_inout_seq)
    test_data_pickled = cloudpickle.dumps(test_data)
    scaler_pickled = cloudpickle.dumps(scaler)

    return train_inout_seq_pickled, test_data_pickled, scaler_pickled

@task
def training(train_inout_seq_pickled):
    import time
    import math
    import cloudpickle
    import torch
    import torch.nn as nn
    import numpy as np

    logger = prefect.context.get("logger")
    train_inout_seq = cloudpickle.loads(train_inout_seq_pickled)
    logger.info("OK")

    def timeSince(since):
        now = time.time()
        s = now - since
        m = math.floor(s / 60)
        s -= m * 60
        return '%dm %ds' % (m, s)

    class LSTM(nn.Module):
        def __init__(self, input_size=1, hidden_layer_size=20, output_size=1):
            super(LSTM, self).__init__()
            self.hidden_layer_size = hidden_layer_size
            self.lstm = nn.LSTM(input_size, hidden_layer_size)
            self.linear = nn.Linear(hidden_layer_size, output_size)

        def forward(self, input_seq, hidden):
            lstm_out, hidden = self.lstm(input_seq.view(1, 1, -1), hidden)
            predictions = self.linear(lstm_out)

            return predictions[-1], hidden

        def initHidden(self):
            return torch.zeros(1, 1, self.hidden_layer_size), torch.zeros(1, 1, self.hidden_layer_size)

    def train(input_tensor, target_tensor, optimizer):
        hidden = lstm.initHidden()
        lstm.zero_grad()
        loss = 0

        for i in range(len(input_tensor)):
            output, hidden = lstm(torch.FloatTensor(np.array([input_tensor[i]])).view(1, 1, -1), hidden)
            l = criterion(output, target_tensor[i])
            loss += l

        loss.backward()
        optimizer.step()

        return output, loss.item() / len(input_tensor), hidden

    lstm = LSTM()
    criterion = nn.MSELoss()
    learning_rate = 0.001
    optimizer = torch.optim.Adam(lstm.parameters(), lr=learning_rate)

    n_iters = 10
    print_every = 1
    plot_every = 10
    all_losses = []
    total_loss = 0  # Reset every plot_every iters

    start = time.time()

    for iter in range(1, n_iters + 1):
        for example in train_inout_seq:
            output, loss, hidden = train(example[0], example[1], optimizer)
            total_loss += loss

        if iter % print_every == 0:
            logger.info('Epoch ' + str(iter) + ' %s (%d %d%%) %.4f' % (timeSince(start), iter, iter / n_iters * 100, loss))

        if iter % plot_every == 0:
            all_losses.append(total_loss)
            total_loss = 0

    lstm_pickled = cloudpickle.dumps(lstm)
    return lstm_pickled

@task
def evaluation(train_inout_seq_pickled, lstm_pickled, test_data_pickled, scaler_pickled):
    import time
    import math
    import cloudpickle
    import torch
    import torch.nn as nn
    import numpy as np
    from sklearn.preprocessing import MinMaxScaler

    def mean_absolute_percentage_error(forecast, target):
        return sum(abs((target - forecast) / target)) * (100 / 12)

    logger = prefect.context.get("logger")

    train_inout_seq = cloudpickle.loads(train_inout_seq_pickled)
    lstm = cloudpickle.loads(lstm_pickled)
    test_data = cloudpickle.loads(test_data_pickled)
    scaler = cloudpickle.loads(scaler_pickled)

    lstm.eval()
    hidden = lstm.initHidden()
    predictions = []
    with torch.no_grad():
        for i in range(len(train_inout_seq[-1][1])):
            output, hidden = lstm(torch.FloatTensor(np.array([train_inout_seq[-1][1][i]])).view(1, 1, -1), hidden)
        predictions.append(output.tolist()[0][0])
        for i in range(11):
            output, hidden = lstm(output.view(1, 1, -1), hidden)
            predictions.append(output.tolist()[0][0])

    actual_predictions = scaler.inverse_transform(np.array(predictions).reshape(-1, 1)).squeeze()
    logger.info(str(mean_absolute_percentage_error(actual_predictions, test_data)))


flow = Flow("try-flow")
# flow.set_dependencies(training, keyword_tasks={"path": preprocessing})
# flow.set_dependencies(evaluation, keyword_tasks={"path": training})
flow.set_dependencies(
        task=preprocessing, downstream_tasks=[training]
    )
flow.set_dependencies(
        task=training, upstream_tasks=[preprocessing], downstream_tasks=[evaluation]
    )


with Flow("try-flow") as flow:
    train_inout_seq_pickled, test_data_pickled, scaler_pickled = preprocessing
    lstm_pickled = training(train_inout_seq_pickled)
    evaluation(train_inout_seq_pickled, lstm_pickled, test_data_pickled, scaler_pickled)

flow.storage = gitlab_storage
flow.run_config = kubernetes_run_config
flow.executor = local_executor

